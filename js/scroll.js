(function($){
  $(document).ready(function(){
    var $single_page_document = $('html, body');
    var $single_page_wrapper = $(".single_page_wrapper");
    $single_page_wrapper.css("clear", "both");
    $(".single_page", $single_page_wrapper).css("overflow", "inherit");
    var $menu_element = $(Drupal.settings.single_page.menu_element);
    var easing = Drupal.settings.single_page.easing;
    $("li:first", $menu_element).addClass("active");
    $("li:first a", $menu_element).addClass("active");
    var target_offset = $(".single_page_wrapper:first").offset();
    var target_top = target_offset.top;
    $single_page_document.animate({
      scrollTop:target_top
    }, 500);
    var anchor = location.hash;
    if(anchor != ""){
      var current_item = $(anchor);
      var index = $single_page_wrapper.index(current_item);
      $("li", $menu_element).removeClass("active");
      $("li a", $menu_element).removeClass("active");
      $("li:eq(" + index + ")", $menu_element).addClass("active");
      $("li:eq(" + index + ") a", $menu_element).addClass("active");
      var target_offset = $(anchor).offset();
      var target_top = target_offset.top;
      $single_page_document.animate({
        scrollTop:target_top
      }, 500);
    }
    $("a", $menu_element).click(function(event){
      $("li", $menu_element).removeClass("active");
      $("li a", menu_element).removeClass("active");
      $(this).addClass("active");
      $(this).parent().addClass("active");
      // Prevent the default action for the click event.
      event.preventDefault();

      // Get the full url - like mysitecom/index.htm#home.
      var full_url = this.href;

      // Split the url by # and get the anchor target name - home in mysitecom/index.htm#home.
      var parts = full_url.split("#");
      var trgt = parts[1];

      // Get the top offset of the target anchor.
      var target_offset = $("#" + trgt).offset();
      var target_top = target_offset.top;
      // Go to that anchor by setting the body scroll top to anchor top.
      if (easing == 'none') {
        $single_page_document.animate({
          scrollTop:target_top
        }, 500);
      }
      else {
        $single_page_document.animate({
          scrollTop:target_top
        }, {
          duration: 1000,
          easing: easing
        });
      }
    });
  });
})(jQuery);