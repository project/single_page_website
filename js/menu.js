(function($){
  $(document).ready(function(){
    var menu_element = Drupal.settings.single_page.menu_element;
    var basePath = Drupal.settings.basePath;
    var pathPrefix = Drupal.settings.pathPrefix;
    $("a", menu_element).each(function() {
      var anchor = this.href.replace(/http(s)?:\/\/[^\\/]+/g, '').replace(basePath, '');
      this.href = basePath + pathPrefix + "#" + "spw-" + anchor.replace("/","-");
    });
  });
})(jQuery);
