(function($){
  $(document).ready(function(){
    var $single_page_wrapper = $(".single_page_wrapper");
    var $header_element = $(Drupal.settings.single_page.header_element);
    var $footer_element = $(Drupal.settings.single_page.footer_element);
    var $menu_element = $(Drupal.settings.single_page.menu_element);
    $header_element.css({
      'position' : 'fixed',
      'top' : '0',
      'width' : '100%',
      'z-index' : '500'
    });
    $footer_element.css({
      'position' : 'fixed',
      'bottom' : '0',
      'width' : '100%',
      'z-index' : '500'
    });
    var header_height = parseInt($header_element.height()) + parseInt($header_element.css("padding-top")) + parseInt($header_element.css("padding-bottom"));
    var footer_height = parseInt($footer_element.height()) + parseInt($footer_element.css("padding-top")) + parseInt($footer_element.css("padding-bottom"));
    var menu_height = parseInt($menu_element.height()) + parseInt($menu_element.css("padding-top")) + parseInt($menu_element.css("padding-bottom"));
    var window_height = $(window).height();
    $single_page_wrapper.css("padding-top",header_height+menu_height);
    if ($footer_element.length > 0){
      $(".single_page", $single_page_wrapper).height(window_height-header_height-footer_height-menu_height-footer_height);
    }
    else{
      $(".single_page", $single_page_wrapper).height(window_height-header_height-footer_height-menu_height);
    }
    $single_page_wrapper.height(window_height-header_height);
    $(".sidebar").hide();
    $(".sidebar-first #content .section").css("padding-left", "0");
    $menu_element.css({
      'position' : 'fixed',
      'top' : header_height,
      'width' : '100%',
      'z-index' : '500'
    });
  });
})(jQuery);
