(function($){
  $(document).ready(function(){
    var menu_element = Drupal.settings.single_page.menu_element;
    $("a", menu_element).click(function(){
      $(".colorbox").colorbox();
    });
  });
})(jQuery);
